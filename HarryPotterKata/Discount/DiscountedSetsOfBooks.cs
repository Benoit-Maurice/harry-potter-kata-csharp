﻿using System.Collections.Generic;
using System.Linq;

namespace HarryPotterKata.Discount
{
    public class DiscountedSetsOfBooks
    {
        private List<DiscountedSetOfBooks> sets = new List<DiscountedSetOfBooks>();

        public DiscountedSetsOfBooks(IEnumerable<DiscountedSetOfBooks> sets)
        {
            this.sets = sets.ToList();
        }

        public Discounts Discounts()
        {
            var discounts = new Discounts();

            foreach (var set in sets)
            {
                set.Fill(discounts);
            }

            return discounts;
        }

        public Cart NewCartWithNotDiscountedBooks()
        {
            var cart = new Cart();

            foreach(var set in sets)
            {
                set.Fill(cart);
            }

            return cart;
        }
    }
}