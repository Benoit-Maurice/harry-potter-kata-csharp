﻿namespace HarryPotterKata.Discount
{
    public class NotDiscountedSetOfBooks : DiscountedSetOfBooks
    {
        public NotDiscountedSetOfBooks(SetOfBooks set)
            : base(0, set) { }

        public override void Fill(Discounts discounts) { }

        public override void Fill(Cart cart)
        {
            cart.Add(books);
        }

        public override string ToString()
        {
            return $"{base.ToString()} => Not Discounted";
        }
    }
}