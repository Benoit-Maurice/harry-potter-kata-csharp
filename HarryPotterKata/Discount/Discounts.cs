﻿using System;
using System.Collections.Generic;

namespace HarryPotterKata.Discount
{
    public class Discounts : IEquatable<Discounts>, IComparable<Discounts>
    {
        private List<DiscountedSetOfBooks> discounts = new List<DiscountedSetOfBooks>();

        public static Discounts Empty() => new Discounts();

        public static Discounts Max() => new MaxDiscounts();

        public int CompareTo(Discounts other)
        {
            return Total().CompareTo(other.Total());
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Discounts);
        }

        public decimal ComputeFullDiscount(decimal amount)
        {
            var total = Total();
            return total.Times(amount);
        }

        public bool Equals(Discounts other)
        {
            if(other == null)
            {
                return false;
            }

            var total = Total();
            var otherTotal = other.Total();
            return total.Equals(otherTotal);
        }

        public override int GetHashCode()
        {
            return Total().GetHashCode();
        }

        public void Add(DiscountedSetOfBooks discount)
        {
            discounts.Add(discount);
        }

        public void AddAll(Discounts additionalDiscounts)
        {
            var discountsToAdd = additionalDiscounts.discounts;
            discounts.AddRange(discountsToAdd);
        }

        protected virtual DiscountTotal Total()
        {
            var total = new DiscountTotal();

            Action<DiscountedSetOfBooks> aggregateTotal = d => d.Aggregate(total);
            discounts.ForEach(aggregateTotal);

            return total;
        }

        public override string ToString()
        {
            return Total().ToString();
        }

        private class MaxDiscounts : Discounts
        {
            private decimal maxDiscountAmount = decimal.MaxValue;

            protected override DiscountTotal Total()
            {
                var total = new DiscountTotal();
                total.Add(maxDiscountAmount);
                return total;
            }
        }
    }

}